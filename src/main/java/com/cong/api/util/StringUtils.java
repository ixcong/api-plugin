package com.cong.api.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串通用类
 */
public class StringUtils {

    /**
     * 检查是否是个不为空的 ,返回不为空的 否则返回obj1
     * @param obj
     * @param obj1
     * @return
     */
    public static Object objectNotNull(Object obj, Object obj1) {
        return obj != null && !"".equals(obj) ? obj : obj1;
    }


    public static String notNull(String str) {
        return str == null ? "" : str;
    }

    public static String format(String str, Object... args) {
        String result = str;
        Pattern p = Pattern.compile("\\{(\\d+)\\}");
        Matcher m = p.matcher(str);

        while(m.find()) {
            int index = Integer.parseInt(m.group(1));
            if (index < args.length) {
                result = result.replace(m.group(), objectNotNull(args[index], "").toString());
            }
        }

        return result;
    }




    public static boolean isNull(String str) {
        return str == null;
    }

    public static boolean isEmpty(String... str) {
        String[] var4 = str;
        int var3 = str.length;

        for(int var2 = 0; var2 < var3; ++var2) {
            String s = var4[var2];
            if (isNull(s) || notNull(s).trim().length() < 1) {
                return true;
            }
        }

        return false;
    }

    public static String toDayString(){
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("YYYY-MM-dd");
        return  sdf.format(date);
    }
}

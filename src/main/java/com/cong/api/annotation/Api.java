package com.cong.api.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Api {
    String createtime() default "";

    String author() default "";

    String other() default "";

    String desc() default "";

    String name() default "";

    Rule[] params() default {};

    Rule[] returns() default {};

    Demo demo() default @Demo;

    Auth[] auth() default {};

    String remark() default "";
}
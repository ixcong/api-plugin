package com.cong.api.annotation;


import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Domain {
    String value() default "";

    String alias() default "";

    String desc() default "";
}

package com.cong.api.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Rule {
    String name() default "";

    String type() default "string";

    String desc() default "";

    String defaultVal() default "";

    boolean required() default false;

}

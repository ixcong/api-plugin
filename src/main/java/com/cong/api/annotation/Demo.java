package com.cong.api.annotation;

public @interface Demo {
    String param() default "";

    String error() default "";

    String success() default "";
}

package com.cong.api.bean;

/**
 * 〈参数实体〉<br>
 *     用于参数接受或者参数还回
 *
 * @author hucong
 * @create 2018/4/23 0023
 */
public class MyParam {
    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数类型
     */
    private String type;
    /**
     * 参数描述
     */
    private String desc;

    /**
     * 是否必须
     */
    private boolean required;

    /**
     * 默认值
     */
    private String defaultVal;

    public MyParam() {

    }

    public MyParam(String name, String type, String desc) {
        this.name = name;
        this.type = type;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDefaultVal() {
        return defaultVal;
    }

    public void setDefaultVal(String defaultVal) {
        this.defaultVal = defaultVal;
    }

}
